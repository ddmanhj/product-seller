const express = require("express");
const path = require("path");
const app = express();
require("dotenv").config();

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

//Controller/error.js
const errorController = require("./controllers/error");
const sequelize = require("./util/database");

//sử dụng body-parser(phân tích và xử lý dữ liệu trong phần body của request)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//Khai báo để ra thư mục gốc => Có thể tìm thấy css ở public
app.use(express.static(path.join(__dirname, "public")));

// Hỗ trợ template egine
app.set("view engine", "ejs");
app.set("views", "views");

//filter route path
app.use("/admin", adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

sequelize
  .sync()
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  });

app.listen(1108);
